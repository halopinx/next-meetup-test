import { Fragment } from 'react';
import Head from 'next/head';
import { MongoClient } from 'mongodb';
import MeetupList from '../components/meetups/MeetupList';

function HomePage(props){
    return (
        <Fragment>
            <Head>
                <title>React Meetups</title>
                <meta name="description" content="A highly active react meetup communities"/>
            </Head>
            <MeetupList meetups={props.meetups} />
        </Fragment>
    )
}

export async function getServerSideProps(context) {
    const req = context.req;
    const res = context.res;


    const client = await MongoClient.connect('mongodb+srv://user_react:MHYz3aXPNY999Av@cluster0.fjszh.mongodb.net/meetups?retryWrites=true&w=majority');

    const db = client.db();
    const meetupCollection = db.collection('meetups');

    const meetups = await meetupCollection.find().toArray();

    client.close();

    return {
        props: {
            meetups: meetups.map(meetup => ({
                title: meetup.title,
                address: meetup.address,
                image: meetup.image,
                id: meetup._id.toString()
            }))
        }
    }
}

// export async function getStaticProps() {
//   return {
//       props: {
//           meetups: DUMMY_MEETUPS
//       },
//       revalidate: 10
//   }
// }

export default HomePage;